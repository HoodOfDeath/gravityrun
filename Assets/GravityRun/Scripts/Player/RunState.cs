﻿using UnityEngine;

public class RunState : PlayerState
{
    readonly private int jumpForce = 20;
    private Vector3 jumpVector;

    public RunState(Player player) : base(player)
    {
    }

    public override void Action()
    {
        if (_player.isGrounded)
        {
            jumpVector = Physics.gravity.normalized * jumpForce * -1;
            _player.playerRigidbody.AddForce(jumpVector, ForceMode.Impulse);
        }
    }
}
