﻿public abstract class PlayerState
{
    protected Player _player;

    public PlayerState(Player player)
    {
        _player = player;
    }

    public abstract void Action();
}

public enum PlayerStates
{
    GravityState,
    RunState
}