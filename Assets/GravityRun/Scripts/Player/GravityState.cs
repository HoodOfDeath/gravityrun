﻿using System.Collections.Generic;
using System.Collections;
using UnityEngine;

public class GravityState : PlayerState
{
    private readonly float RotationSpeed = 360f;
    Quaternion _rotation;

    public GravityState(Player player) : base(player)
    {
        _rotation = new Quaternion();
        _rotation.eulerAngles = new Vector3(180, 180, 0);
    }

    public override void Action()
    {
        if (_player.isGrounded)
        {
            Physics.gravity *= -1;
            _player.StartCoroutine(Rotate());
        }
    }

    private IEnumerator Rotate()
    {
        Quaternion endRotaion = _player.transform.rotation * _rotation;

        while (_player.transform.rotation != endRotaion)
        {
            _player.transform.rotation = Quaternion.RotateTowards(_player.transform.rotation, endRotaion, RotationSpeed * Time.deltaTime);
            yield return null;
        }

    }
}
