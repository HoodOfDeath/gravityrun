﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

public class CameraControl : MonoBehaviour
{
    [Inject]
    private Player _player;
    private Vector3 _offset;
    private Vector3 _startPosition;

    private void Start()
    {
        _offset = _player.transform.position - transform.position;
        _startPosition = transform.position;
    }

    private void FixedUpdate()
    {
        Vector3 newPosition = _player.transform.position - _offset;
        newPosition.y = _startPosition.y;
        transform.position = newPosition;
    }
}
