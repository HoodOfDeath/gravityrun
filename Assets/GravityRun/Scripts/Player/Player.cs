﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

[RequireComponent(typeof(Rigidbody),typeof(Collider),typeof(Animator))]
public class Player : MonoBehaviour
{
    [SerializeField] new Renderer renderer;

    public bool isDead
    {
        get
        {
            bool result = !renderer.isVisible;
            return result;
        }
    }

    public bool HoldOn
    {
        get; set;
    }

    [HideInInspector] public bool isGrounded;
    [HideInInspector] public Rigidbody playerRigidbody;


    private readonly float Speed = 15f;
    private readonly float Acceleration = 7.5f;
    private readonly int GroundLayer = 9;

    private Animator _animator;
    private PlayerState _currentState;
    private Vector3 _startPosition;
    private Quaternion _startRotation;
    private Coroutine _stateCoroutine;

    [Inject]
    private void Construct(GravityState gravityState)
    {
        _currentState = gravityState;
        playerRigidbody = GetComponent<Rigidbody>();
        _startPosition = transform.position;
        _startRotation = transform.rotation;
        HoldOn = true;
        _animator = GetComponent<Animator>();
    }
    
    private void Update()
    {
        HandleAnimation();

#if UNITY_EDITOR
        if (Input.GetButtonDown("Jump") && !HoldOn)
        {
            Action();
        }
#endif

        if (Input.touchCount == 0)
            return;

        var touch = Input.GetTouch(0);
        if (touch.phase == TouchPhase.Began && !HoldOn)
        {
            Action();
        }
    }

    private void FixedUpdate()
    {
        if (HoldOn)
        {
            return;
        }

        Run();
    }

    public void Action()
    {
        _currentState.Action();
    }

    public void ResetPosition()
    {
        transform.position = _startPosition;
        transform.rotation = _startRotation;
    }

    public void RunStateRourine(IEnumerator routine)
    {
         _stateCoroutine = StartCoroutine(routine);
    }

    public void StopStateRoutine(string routine)
    {
        if (_stateCoroutine != null)
        {
            StopCoroutine(_stateCoroutine);
        }
    }

    public void SetState(PlayerState state)
    {
        _currentState = state;
    }

    private void Run()
    {
        if (playerRigidbody.velocity.x < Speed && isGrounded)
        {
            playerRigidbody.velocity = new Vector3(Speed , 0, 0);
        }
    }

    private void HandleAnimation()
    {
        _animator.SetFloat("Speed", playerRigidbody.velocity.x / (Speed/3));
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.layer == GroundLayer)
        {
            isGrounded = false;
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.layer == GroundLayer)
        {
            isGrounded = true;
        }
    }

    public class Settings
    {
        public float speed = 15f;
    }
}
