﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum PlatformType
{
    Gap, Left, Right, Full
}

public abstract class BlockBuilder
{
    public BlockBuilder() { }

    public abstract BlockBuilder Reset();
    public abstract BlockBuilder SetUpperPlatform(PlatformType type);
    public abstract BlockBuilder SetLowerPlatform(PlatformType type);
    public abstract BlockBuilder SetBonus(BonusType bonus);
    public abstract GameObject GetResult();
}