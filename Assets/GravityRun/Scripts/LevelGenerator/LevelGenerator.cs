﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

public class LevelGenerator : MonoBehaviour
{
    private enum Sides { Top, Bottom}

    private BlockBuilder _builder;
    private Player _player;

    private readonly float BlockOffset = 5f;
    private readonly float MinDistanceToPlayer = 80f;
    private readonly int defaultProbabilityOfSideChange = 70;

    private GameObject _lastBlock;
    private List<GameObject> _blocks = new List<GameObject>(30);
    private List<PlatformType> _upperPlatforms, _lowerPlatforms;
    private int probabilityOfSideChange = 70;
    private Sides currentSide = Sides.Bottom;

    public bool HoldOn
    {
        get; set;
    }

    private bool isBlockRequired
    {
        get
        {
            return _lastBlock.transform.position.x - _player.transform.position.x < MinDistanceToPlayer;
        }
    }

    private bool isBonusRequired
    {
        get
        {
            int chance = Random.Range(0, 5);
            return chance == 2 ? true : false;
        }
    }

    private BonusType newBonus
    {
        get
        {
            int chance = Random.Range(0, 5);
            BonusType result;
            switch (chance)
            {
                case 0:
                    result = BonusType.RunState;
                    break;
                case 1:
                    result = BonusType.CoinsUp;
                    break;
                default:
                    result = BonusType.Coin;
                    break;
            }

            return result;
        }
    }

    [Inject]
    private void Construct(ClassicBlockBuilder builder, Player player)
    {
        _builder = builder;
        _player = player;
    }

    // Start is called before the first frame update
    private void Start()
    {
        InitializeLevel();

    }//*/

    public void InitializeLevel()
    {
        while (_blocks.Count > 0)
        {
            CleanUpBlock(0);
        }

        _upperPlatforms = new List<PlatformType>();
        _lowerPlatforms = new List<PlatformType>();
        _builder.Reset();

        _builder.SetLowerPlatform(PlatformType.Full).SetUpperPlatform(PlatformType.Full);
        _lastBlock = _builder.GetResult();
        _lastBlock.transform.SetParent(this.transform);
        _lastBlock.transform.position = new Vector3(BlockOffset * -1, 0, 0);
        _blocks.Add(_lastBlock);

        for (int i = 0; i < 7; i++)
        {
            _lastBlock = Instantiate(_lastBlock);
            _lastBlock.transform.SetParent(this.transform);
            _lastBlock.transform.position = new Vector3(BlockOffset * i, 0, 0);
            _blocks.Add(_lastBlock);
        }
    }

    private void GenerateLevel()
    {
        while (isBlockRequired)
        {
            _builder.Reset();

            RandomizeBlock();

            GameObject newBlock = _builder.GetResult();
            newBlock.transform.SetParent(this.transform);
            newBlock.transform.position = _lastBlock.transform.position + Vector3.right * BlockOffset;

            _lastBlock = newBlock;
            if (_blocks.Count >= _blocks.Capacity && !isBlockRequired)
            {
                CleanUpBlock(0);
            }
            _blocks.Add(_lastBlock);
        }
    }

    private void CleanUpBlock(int blockIndex)
    {
        Destroy(_blocks[blockIndex]);
        _blocks.RemoveAt(blockIndex);
    }

    private void RandomizeBlock()
    {
        if (_upperPlatforms.Count == 0)
        {
            GenereteFloorsSequences();
            ChooseSide();
        }

        _builder.SetUpperPlatform(_upperPlatforms[0]).SetLowerPlatform(_lowerPlatforms[0]);

        if (isBonusRequired)
        {
            _builder.SetBonus(newBonus);
        }

        _upperPlatforms.RemoveAt(0);
        _lowerPlatforms.RemoveAt(0);
    }

    private void GenereteFloorsSequences()
    {
        int distanceToNextHole = Random.Range(1, 8);
        int sizeOfHole = Random.Range(1, 4);

        distanceToNextHole = Mathf.Max((sizeOfHole + 1), distanceToNextHole);

        if (currentSide == Sides.Bottom)
        {
            _lowerPlatforms = BuildSequence(distanceToNextHole, sizeOfHole);
            _upperPlatforms = BuildSequence(distanceToNextHole, 0);
        }
        else
        {
            _upperPlatforms = BuildSequence(distanceToNextHole, sizeOfHole);
            _lowerPlatforms = BuildSequence(distanceToNextHole, 0);
        }
    }

    private List<PlatformType> BuildSequence(int fullSize, int holeSize)
    {
        fullSize = (fullSize % 2 == 1) ? fullSize + 1 : fullSize;
        List<PlatformType> result = new List<PlatformType>(fullSize/2);

        int fullGaps = holeSize / 2;
        int smallGap = holeSize % 2;
        int fullPlatform = (fullSize - holeSize) / 2;

        for (int i = 0; i < fullGaps; i++)
        {
            result.Add(PlatformType.Gap);
        }

        if (smallGap != 0)
        {
            result.Add(PlatformType.Right);
        }

        for (int i = 0; i < fullPlatform; i++)
        {
            result.Add(PlatformType.Full);
        }

        return result;
    }

    private void ChooseSide()
    {
        int stayOnSideChance = Random.Range(0, 100);

        if (stayOnSideChance < probabilityOfSideChange)
        {
            currentSide = (currentSide == Sides.Bottom) ? Sides.Top : Sides.Bottom;
            probabilityOfSideChange = defaultProbabilityOfSideChange;
        }
        else
        {
            probabilityOfSideChange += 10;
        }
    }

    private void FixedUpdate()
    {
        if (HoldOn)
        {
            return;
        }

        GenerateLevel();
    }
}
