﻿using System;
using UnityEngine;
using Zenject;

public class ClassicBlockBuilder : BlockBuilder
{
    private readonly Settings _settings;
    private readonly BonusFactory _bonusFactory;

    private GameObject _classicBlock;
    private GameObject _upperPart, _lowerPart;
    private Vector3 _upperPartPosition, _lowerPartPosition;
    private BonusType _bonusType;
    private bool _bonusIsSet;

    public ClassicBlockBuilder(Settings settings, BonusFactory bonusFactory)
    {
        _settings = settings;
        _bonusFactory = bonusFactory;
        _bonusIsSet = false;
    }

    public override BlockBuilder Reset()
    {
        _classicBlock = null;
        _upperPart = null;
        _upperPartPosition = Vector3.zero;
        _lowerPart = null;
        _lowerPartPosition = Vector3.zero;
        _bonusIsSet = false;

        return this;
    }

    public override BlockBuilder SetBonus(BonusType bonus)
    {
        _bonusType = bonus;
        _bonusIsSet = true;
        return this;
    }

    public override BlockBuilder SetLowerPlatform(PlatformType type)
    {
        switch (type)
        {
            case PlatformType.Gap:
                break;
            case PlatformType.Full:
                SetFullLowerPlatform();
                break;
            case PlatformType.Left:
                SetSmallLowerPlatform(type);
                break;
            case PlatformType.Right:
                SetSmallLowerPlatform(type);
                break;
            default:
                throw new ArgumentOutOfRangeException("type", type, null);
        }

        return this;
    }

    private BlockBuilder SetSmallLowerPlatform(PlatformType pos)
    {
        _lowerPart = _settings.halfPlatform;

        Vector3 position = SetSideOffset(pos);
        position.y = _settings.verticalOffset * -1;
        
        _lowerPartPosition = position;
        return this;
    }

    private BlockBuilder SetFullLowerPlatform()
    {
        _lowerPart = _settings.fullPlatform;
        Vector3 position = Vector3.down * _settings.verticalOffset;
        
        _lowerPartPosition = position;
        return this;
    }
    
    public override BlockBuilder SetUpperPlatform(PlatformType type)
    {
        switch (type)
        {
            case PlatformType.Gap:
                break;
            case PlatformType.Full:
                SetFullUpperPlatform();
                break;
            case PlatformType.Left:
                SetSmallUpperPlatform(type);
                break;
            case PlatformType.Right:
                SetSmallUpperPlatform(type);
                break;
            default:
                throw new ArgumentOutOfRangeException("type", type, null);
        }

        return this;
    }

    private BlockBuilder SetSmallUpperPlatform(PlatformType pos)
    {
        _upperPart = _settings.halfPlatform;

        Vector3 position = SetSideOffset(pos);
        position.y = _settings.verticalOffset;

        _upperPartPosition = position;
        return this;
    }

    private BlockBuilder SetFullUpperPlatform()
    {
        _upperPart = _settings.fullPlatform;
        Vector3 position = Vector3.up * _settings.verticalOffset;

        _upperPartPosition = position;
        return this;
    }

    public override GameObject GetResult()
    {
        _classicBlock = new GameObject("ClassicBlock");

        if (_upperPart != null)
        {
            GameObject uPart = GameObject.Instantiate(_upperPart);
            uPart.transform.SetParent(_classicBlock.transform);
            uPart.transform.localPosition = _upperPartPosition;
        }

        if (_lowerPart != null)
        {
            GameObject lPart = GameObject.Instantiate(_lowerPart);
            lPart.transform.SetParent(_classicBlock.transform);
            lPart.transform.localPosition = _lowerPartPosition;
        }

        if (_bonusIsSet)
        {
            IBonus bonus = _bonusFactory.Create(_bonusType);
            bonus.SetParent(_classicBlock.transform);
            bonus.SetRelativePosition(Vector3.zero);
        }

        return _classicBlock;
    }

    private Vector3 SetSideOffset(PlatformType pos)
    {
        Vector3 result = Vector3.zero;
        float leftOffset = -_settings.horizontalOffset;
        float rightOffset = _settings.horizontalOffset;

        switch (pos)
        {
            case PlatformType.Left:
                result.x = leftOffset;
                break;
            case PlatformType.Right:
                result.x = rightOffset;
                break;
            default:
                throw new ArgumentOutOfRangeException("pos", pos, "The type neither Left or Right");
        }

        return result;
    }

    [Serializable]
    public class Settings
    {
        public GameObject halfPlatform;
        public GameObject fullPlatform;
        public float verticalOffset;
        public float horizontalOffset;
        public float blockWidth;
    }
}
