﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

public class UIController : MonoBehaviour
{

    [SerializeField] private GameObject mainMenuUI;
    [SerializeField] private GameObject inGameUI;
    [SerializeField] private GameObject gameOverUI;

    [Space]
    [SerializeField] private Text score;
    [SerializeField] private Text bestScore;
    [SerializeField] private Text coins;

    [Space]
    [SerializeField] private GameObject bonusTimeUI;
    [SerializeField] private Text leftTime;

    [Space]
    [SerializeField] private Text bestScoreResult;
    [SerializeField] private Text finalScore;
    [SerializeField] private Text totalCoins;

    private GameController _gameController;
    private ScoreSystem _scoreSystem;

    private Coroutine _countdownCoroutine;

    [Inject]
    private void Construct(GameController gameController, ScoreSystem scoreSystem)
    {
        _gameController = gameController;
        _scoreSystem = scoreSystem;

        _gameController.OnStateChange += ChangeState;
        _scoreSystem.OnScoreChanged += UpdateScore;
        _scoreSystem.OnBestScoreChanged += UpdateBestScore;
        _scoreSystem.OnCoinsChanged += UpdateCoins;
    }

    private void ChangeState(GameStates newState)
    {
        bestScore.text = $"BEST {_scoreSystem.BestScore} m";
        switch (newState)
        {
            case GameStates.Menu:
                mainMenuUI.SetActive(true);
                inGameUI.SetActive(false);
                gameOverUI.SetActive(false);
                break;
            case GameStates.Playing:
                mainMenuUI.SetActive(false);
                inGameUI.SetActive(true);
                gameOverUI.SetActive(false);
                bonusTimeUI.SetActive(false);
                coins.text = "0";
                break;
            case GameStates.GameOver:
                StopAllCoroutines();
                bestScoreResult.text = $"Best score: {_scoreSystem.BestScore} m";
                finalScore.text = $"Your score: {_scoreSystem.CurrentScore} m";
                totalCoins.text = $"Total coins: {_scoreSystem.TotalCoins}";
                mainMenuUI.SetActive(false);
                inGameUI.SetActive(false);
                gameOverUI.SetActive(true);
                break;
            default:
                throw new System.ArgumentOutOfRangeException("newState", newState, null);
        }
    }
    
    public void PlayGame()
    {
        _gameController.ChangeStateTo(GameStates.Playing);
    }

    public void RestartGame()
    {
        _gameController.ChangeStateTo(GameStates.Menu);
    }

    public void ShowBonusTime(float bonusDuration)
    {
        bonusTimeUI.SetActive(true);
        if (_countdownCoroutine != null)
        {
            StopCoroutine(_countdownCoroutine);
        }

        _countdownCoroutine = StartCoroutine(Countodown(bonusDuration));
    }

    private IEnumerator Countodown(float time)
    {
        float timeLeft = time;
        int previoslyShownTime = Mathf.RoundToInt(timeLeft);
        leftTime.text = previoslyShownTime.ToString();

        while (timeLeft >= 0)
        {
            yield return null;
            timeLeft -= Time.deltaTime;
            int newShownTime = Mathf.RoundToInt(timeLeft);
            if (newShownTime < previoslyShownTime)
            {
                previoslyShownTime = newShownTime;
                leftTime.text = previoslyShownTime.ToString();
            }
        }

        bonusTimeUI.SetActive(false);
    }

    private void UpdateScore(int newScore)
    {
        score.text =  $"{newScore} m";
    }

    private void UpdateBestScore(int newBestScore)
    {
        bestScore.text = "New Best Score";
    }

    private void UpdateCoins(int newCoins)
    {
        coins.text = newCoins.ToString();
    }
}
