﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// The whole purpose of this class is ti run coroutines, which can't be run by desirible script
/// </summary>
public class CoroutineRunner : MonoBehaviour
{
    Dictionary<string, Coroutine> _coroutines = new Dictionary<string, Coroutine>(5);

    public void RunRoutine(IEnumerator routine, string name)
    {
        if (!_coroutines.ContainsKey(name))
        {
            Coroutine coroutine = StartCoroutine(routine);
            _coroutines.Add(name, coroutine);
        }
    }

    public void StopRoutine(string routine)
    {
        if (_coroutines.ContainsKey(routine))
        {
            StopCoroutine(_coroutines[routine]);
            _coroutines.Remove(routine);
        }
    }
}
