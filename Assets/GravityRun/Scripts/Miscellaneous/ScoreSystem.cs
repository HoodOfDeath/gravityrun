﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;
using System;

public class ScoreSystem : ITickable
{
    private const string BestScoreKey = "BestScore";
    private const string TotalCoinsKey = "TotalCoins";

    private int _currentCoins;

    public int BestScore
    {
        get
        {
            bool hasKey = PlayerPrefs.HasKey(BestScoreKey);
            return hasKey ? PlayerPrefs.GetInt(BestScoreKey) : 0;
        }
        private set
        {
            PlayerPrefs.SetInt(BestScoreKey, value);
        }
    }

    public int TotalCoins
    {
        get
        {
            bool hasKey = PlayerPrefs.HasKey(TotalCoinsKey);
            return hasKey ? PlayerPrefs.GetInt(TotalCoinsKey) : 0;
        }
        private set
        {
            PlayerPrefs.SetInt(TotalCoinsKey, value);
        }
    }
    public int CurrentScore { get; private set; }

    public int CurrentRunCoins
    {
        get
        {
            return _currentCoins;
        }
        set
        {
            _currentCoins = _currentCoins + (value - _currentCoins) * CoinsMultiplier;
            OnCoinsChanged?.Invoke(_currentCoins);
        }
    }

    public int CoinsMultiplier
    {
        private get; set;
    }

    public event Action<int> OnScoreChanged;
    public event Action<int> OnCoinsChanged;
    public event Action<int> OnBestScoreChanged;

    private readonly GameController _gameController;
    private readonly Player _player;

    private bool notPlaying = true;

    private ScoreSystem(GameController gameController, Player player)
    {
        _gameController = gameController;
        _player = player;
        CoinsMultiplier = 1;

        _gameController.OnStateChange += ChangeState;
    }

    private void ChangeState(GameStates states)
    {
        switch (states)
        {
            case GameStates.Menu:
                notPlaying = true;
                break;
            case GameStates.Playing:
                CurrentScore = 0;
                _currentCoins = 0;
                notPlaying = false;
                break;
            case GameStates.GameOver:
                notPlaying = true;
                UpdateBestScore();
                UpdateTotalCoins();
                break;
            default:
                break;
        }
    }

    public void Tick()
    {
        if (notPlaying)
        {
            return;
        }

        int newScore = Mathf.FloorToInt(_player.transform.position.x);

        if (newScore >= CurrentScore)
        {
            CurrentScore = newScore;

            OnScoreChanged?.Invoke(CurrentScore);

            if (CurrentScore >= BestScore)
            {
                OnBestScoreChanged?.Invoke(CurrentScore);
            }
        }
    }

    private void UpdateBestScore()
    {
        if (CurrentScore >= BestScore)
        {
            BestScore = CurrentScore;
        }
    }

    private void UpdateTotalCoins()
    {
        TotalCoins += CurrentRunCoins;
    }
}
