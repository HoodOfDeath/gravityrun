﻿using Zenject;

public class GameOverState : GameState
{
    private Player _player;
    private CoroutineRunner _coroutineRunner;

    protected GameOverState(GameController gameController, Player player, CoroutineRunner coroutineRunner) : base(gameController)
    {
        _player = player;
        _coroutineRunner = coroutineRunner;
    }

    public override void OnStateEnter()
    {
        _player.playerRigidbody.isKinematic = true;
        _player.StopAllCoroutines();
        _coroutineRunner.StopAllCoroutines();
    }

    public override void OnStateExit()
    {

    }

    public override void Update()
    {

    }

    public class Factory : PlaceholderFactory<GameOverState>
    {
    }
}
