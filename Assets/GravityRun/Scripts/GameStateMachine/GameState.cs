﻿using System.Collections;
using System.Collections.Generic;

public abstract class GameState
{
    protected readonly GameController GameController;

    protected GameState(GameController gameController)
    {
        GameController = gameController;
    }

    public abstract void OnStateEnter();
    public abstract void Update();
    public abstract void OnStateExit();
}

public enum GameStates
{
    Menu,
    Playing,
    GameOver
}