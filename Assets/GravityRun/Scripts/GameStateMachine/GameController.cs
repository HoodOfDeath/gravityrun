﻿using Zenject;
using System;

public class GameController : IInitializable, ITickable
{
    public event Action<GameStates> OnStateChange; 

    private GameState _currentState;
    private GameStateFactory _container;

    private bool changingState;

    private GameController(GameStateFactory container)
    {
        _container = container;
    }
    

    public void Initialize()
    {
        ChangeStateTo(GameStates.Menu);
    }

    public void Tick()
    {
        if (changingState)
        {
            return;
        }

        _currentState.Update();
    }

    public void ChangeStateTo(GameStates state)
    {
        changingState = true;

        if (_currentState != null)
        {
            _currentState.OnStateExit();
        }

        _currentState = _container.Create(state);

        _currentState.OnStateEnter();

        OnStateChange?.Invoke(state);

        changingState = false;
    }
}