﻿using UnityEngine;
using Zenject;

public class MenuState : GameState
{

    private readonly Vector3 DefaultGravity = Vector3.down * 50;
    private readonly Player _player;
    private readonly PlayerState _basePlayerState;
    private readonly LevelGenerator _levelGenerator;

    protected MenuState(Player player, LevelGenerator levelGenerator, GameController gameController, GravityState gravityState) : base(gameController)
    {
        _player = player;
        _basePlayerState = gravityState;
        _levelGenerator = levelGenerator;
    }

    public override void OnStateEnter()
    {
        _player.SetState(_basePlayerState);
        _player.ResetPosition();
        _player.playerRigidbody.isKinematic = false;

        Physics.gravity = DefaultGravity;

        _levelGenerator.InitializeLevel();

    }

    public override void Update()
    {
    }

    public override void OnStateExit()
    {
    }

    public class Factory : PlaceholderFactory<MenuState>
    {
    }
}
