﻿using Zenject;

public class PlayingState : GameState
{
    private readonly Player _player;
    private readonly LevelGenerator _levelGenerator;

    protected PlayingState(Player player, LevelGenerator levelGenerator, GameController gameController) : base(gameController)
    {
        _player = player;
        _levelGenerator = levelGenerator;
    }

    public override void OnStateEnter()
    {
        _levelGenerator.HoldOn = false;
        _player.HoldOn = false;
    }

    public override void Update()
    {
        if (_player.isDead)
        {
            GameController.ChangeStateTo(GameStates.GameOver);
        }
    }

    public override void OnStateExit()
    {
        _levelGenerator.HoldOn = true;
        _player.HoldOn = true;
    }

    public class Factory : PlaceholderFactory<PlayingState>
    {
    }
}
