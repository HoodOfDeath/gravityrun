﻿using Zenject;
using System;

public class GameStateFactory : IFactory<GameStates, GameState>
{
    private readonly MenuState.Factory _menuStateFactory;
    private readonly PlayingState.Factory _playingStateFactory;
    private readonly GameOverState.Factory _gameOverStateFactory;

    private GameStateFactory(MenuState.Factory menuStateFactory, PlayingState.Factory playingStateFactory, GameOverState.Factory gameOverStateFactory)
    {
        _menuStateFactory = menuStateFactory;
        _playingStateFactory = playingStateFactory;
        _gameOverStateFactory = gameOverStateFactory;
    }

    public GameState Create(GameStates state)
    {
        switch (state)
        {
            case GameStates.Menu:
                return _menuStateFactory.Create();
            case GameStates.Playing:
                return _playingStateFactory.Create();
            case GameStates.GameOver:
                return _gameOverStateFactory.Create();
            default:
                throw new ArgumentOutOfRangeException("state", state, "There is no such a state");
        }
    }
}