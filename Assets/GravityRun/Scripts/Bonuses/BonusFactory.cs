﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

public class BonusFactory : IFactory<BonusType, IBonus>
{
    private readonly Coin.Factory _coinFactory;
    private readonly CoinsUp.Factory _coinsUpFactory;
    private readonly RunStateBonus.Factory _runStateBonusFactory;

    private readonly CoroutineRunner _coroutineRunner;
    private readonly Vector3[] coinsPositions = { new Vector3(2.3f, 0, 0), new Vector3(-2.3f, 0f, 0), new Vector3(0, -2.3f, 0), new Vector3(0, 2.3f, 0), Vector3.zero,
                                                  new Vector3(1.2f, 1.2f, 0), new Vector3(1.2f, -1.2f, 0), new Vector3(-1.2f, 1.2f, 0), new Vector3(-1.2f, -1.2f, 0) };

    private BonusFactory(Coin.Factory coinFactory, CoinsUp.Factory coinsUpFactory, RunStateBonus.Factory runStateBonusFactory, CoroutineRunner coroutineRunner)
    {
        _coinFactory = coinFactory;
        _coinsUpFactory = coinsUpFactory;
        _runStateBonusFactory = runStateBonusFactory;
        _coroutineRunner = coroutineRunner;
    }

    public IBonus Create(BonusType bonusType)
    {
        IBonus result;

        switch (bonusType)
        {
            case BonusType.Coin:
                result = CreateBunchOfCoins(coinsPositions);
                break;
            case BonusType.CoinsUp:
                result = _coinsUpFactory.Create();
                break;
            case BonusType.RunState:
                result = _runStateBonusFactory.Create();
                break;
            default:
                throw new ArgumentOutOfRangeException("bonusType", bonusType, null);
        }
        
        return result;
    }

    private IBonus CreateBunchOfCoins(Vector3[] positions)
    {
        GameObject bunchOfCoins = new GameObject("BunchOfCoins");
        IBonus result = bunchOfCoins.AddComponent<GroupBonusHolder>();
        Coin newCoin;

        for (int i = 0; i < positions.Length; i++)
        {
            newCoin = _coinFactory.Create();
            newCoin.SetParent(bunchOfCoins.transform);
            newCoin.SetRelativePosition( positions[i]);
        }

        return result;
    }
}
