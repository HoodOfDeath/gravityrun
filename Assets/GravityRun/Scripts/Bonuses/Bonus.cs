﻿using UnityEngine;
using System.Collections;

public interface IBonus
{
    void Activate();
    void SetRelativePosition(Vector3 position);
    void SetParent(Transform parent);
}

public interface IPlayerStateBonus : IBonus
{
    IEnumerator SwitchState();
}

public enum BonusType
{
    Coin,
    CoinsUp,
    RunState
}