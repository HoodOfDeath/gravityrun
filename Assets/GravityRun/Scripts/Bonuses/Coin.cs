﻿using UnityEngine;
using Zenject;

[RequireComponent(typeof(Collider), typeof(Renderer))]
public class Coin : MonoBehaviour, IBonus
{
    protected readonly int value = 1;
    private bool _isUsed = false;

    [Inject]
    private ScoreSystem _scoreSystem;

    private void OnTriggerEnter(Collider other)
    {
        if (_isUsed)
        {
            return;
        }
        Activate();
    }

    public void Activate()
    {
        _scoreSystem.CurrentRunCoins += value;
        _isUsed = true;
        Destroy(this.gameObject);
    }

    public void SetParent(Transform parent)
    {
        transform.SetParent(parent);
    }

    public void SetRelativePosition(Vector3 position)
    {
        transform.localPosition = position;
    }
    
    public class Factory : PlaceholderFactory<Coin>
    {
    }
}
