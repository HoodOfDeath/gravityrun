﻿using UnityEngine;
using System.Collections;
using Zenject;

[RequireComponent(typeof(Collider), typeof(Renderer))]
public class CoinsUp : MonoBehaviour, IBonus
{
    private const float BonusDuration = 10f;
    private const int CoinsMultiplier = 2;

    private UIController _UIController;
    private ScoreSystem _scoreSystem;
    private CoroutineRunner _coroutineRunner;
    private bool _isUsed = false;

    [Inject]
    private void Construct(ScoreSystem scoreSystem, UIController UIController, CoroutineRunner coroutineRunner)
    {
        _UIController = UIController;
        _scoreSystem = scoreSystem;
        _coroutineRunner = coroutineRunner;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (_isUsed)
        {
            return;
        }
        Activate();
    }

    public void Activate()
    {
        _coroutineRunner.StopRoutine("CoinsUP");
        _coroutineRunner.RunRoutine(CoinsUP(), "CoinsUP");
        _UIController.ShowBonusTime(BonusDuration);
        _isUsed = true;
        Destroy(this.gameObject);
    }
    
    public void SetParent(Transform parent)
    {
        transform.SetParent(parent);
    }

    public void SetRelativePosition(Vector3 position)
    {
        transform.localPosition = position;
    }

    private IEnumerator CoinsUP()
    {
        _scoreSystem.CoinsMultiplier = 2;
        yield return new WaitForSeconds(BonusDuration);
        _scoreSystem.CoinsMultiplier = 1;
    }

    public class Factory : PlaceholderFactory<CoinsUp>
    {
    }
}
