﻿using UnityEngine;

public class GroupBonusHolder : MonoBehaviour, IBonus
{
    public void Activate()
    {
    }

    public void SetParent(Transform parent)
    {
        transform.SetParent(parent);
    }

    public void SetRelativePosition(Vector3 position)
    {
        transform.localPosition = position;
    }
}
