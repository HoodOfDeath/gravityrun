﻿using UnityEngine;
using System.Collections;
using Zenject;

[RequireComponent(typeof(Collider),typeof(Renderer))]
public class RunStateBonus : MonoBehaviour, IPlayerStateBonus
{
    private const float BonusDuration = 10f;

    private Player _player;
    private GravityState _gravityState;
    private RunState _runState;
    private UIController _UIController;
    private bool _isUsed = false;

    [Inject]
    private void Construct(Player player, GravityState gravityState, RunState runState, UIController UIController)
    {
        _player = player;
        _gravityState = gravityState;
        _runState = runState;
        _UIController = UIController;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (_isUsed)
        {
            return;
        }

        Activate();
    }

    public void Activate()
    {
        _player.StopStateRoutine("SwitchState");
        _player.RunStateRourine(SwitchState());
        _UIController.ShowBonusTime(BonusDuration);
        _isUsed = true;
        Destroy(this.gameObject);
    }

    public void SetParent(Transform parent)
    {
        transform.SetParent(parent);
    }

    public void SetRelativePosition(Vector3 position)
    {
        transform.localPosition = position;
    }

    public IEnumerator SwitchState()
    {
        _player.SetState(_runState);
        yield return new WaitForSeconds(BonusDuration);
        _player.SetState(_gravityState);
    }

    public class Factory : PlaceholderFactory<RunStateBonus>
    {
    }
}
