using System;
using UnityEngine;
using Zenject;

public class GravityRunGameInstaller : MonoInstaller
{
    [Inject] private Settings _bonusFactorySettings;
    
    public override void InstallBindings()
    {
        Container.BindInterfacesAndSelfTo<ScoreSystem>().AsSingle();
        LevelGeneratorInstall();
        PlayerStatesInstall();
        GameStateMachineInstall();
    }

    private void LevelGeneratorInstall()
    {
        Container.BindInterfacesAndSelfTo<ClassicBlockBuilder>().AsSingle();


        Container.BindFactory<Coin, Coin.Factory>().FromComponentInNewPrefab(_bonusFactorySettings.coinPrefab)
            .WithGameObjectName("Coin").AsSingle();
        Container.BindFactory<CoinsUp, CoinsUp.Factory>().FromComponentInNewPrefab(_bonusFactorySettings.coinsUpPrefab)
            .WithGameObjectName("CoinsUp").AsSingle();
        Container.BindFactory<RunStateBonus, RunStateBonus.Factory>().FromComponentInNewPrefab(_bonusFactorySettings.runStateBonusPrefab)
            .WithGameObjectName("RunStateBonus").AsSingle();
        Container.BindInterfacesAndSelfTo<BonusFactory>().AsSingle();
    }

    private void PlayerStatesInstall()
    {
        Container.BindInterfacesAndSelfTo<GravityState>().AsSingle();
        Container.BindInterfacesAndSelfTo<RunState>().AsSingle();
    }

    private void GameStateMachineInstall()
    {
        Container.BindInterfacesAndSelfTo<GameController>().AsSingle();
        Container.BindInterfacesAndSelfTo<GameStateFactory>().AsSingle();
        Container.BindFactory<MenuState, MenuState.Factory>().AsSingle();
        Container.BindFactory<PlayingState, PlayingState.Factory>().AsSingle();
        Container.BindFactory<GameOverState, GameOverState.Factory>().AsSingle();
    }

    [Serializable]
    public class Settings
    {
        public GameObject runStateBonusPrefab;
        public GameObject coinPrefab;
        public GameObject coinsUpPrefab;
    }
}