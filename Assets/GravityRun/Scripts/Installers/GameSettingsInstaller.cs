﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

[CreateAssetMenu(menuName = "GravityRun/Game Settings")]
public class GameSettingsInstaller : ScriptableObjectInstaller<GameSettingsInstaller>
{
    [SerializeField] private ClassicBlockBuilder.Settings classicBlockSettings;
    [SerializeField] private GravityRunGameInstaller.Settings bonusFactorySettings;

    public override void InstallBindings()
    {
        Container.BindInstance(classicBlockSettings);
        Container.BindInstance(bonusFactorySettings);
    }
}
